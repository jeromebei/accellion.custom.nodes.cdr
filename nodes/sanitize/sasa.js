const path = require('path');
const fs = require('fs');
const request = require('request');
const rimraf = require('rimraf');
const requirex = require('./requirex.js');
//==============================================================================
const Trail=requirex("trail",["node-red-contrib-accellion","accellion.node-red"]);
//==============================================================================
const Threads=require("./threads.js");
const Sanitizer=require("./sanitizer.js");
//==============================================================================
module.exports = function(RED) {
  //============================================================================
  SCAN_STATE={"UNKNOWN":0, "INQUEUE":1,"INSCAN":2,"FINISHED":3,"RECEIVED":4,"PRESCAN":5};
  SCAN_RESULT={"OK":0, "DROP":1,"RECONSTRUCTED":2,"ERROR":3};
  //============================================================================
  function Acc_Sanitize_Sasa(config) {
    RED.nodes.createNode(this,config);
    var node=this;
    var sanitizer = new Sanitizer(config,node,RED);
    var sanitizePool = Threads.createPool(node.id+"_sanitize", config.sanitizePool || 10).run();
    //--------------------------------------------------------------------------
    // INPUT
    //--------------------------------------------------------------------------
    node.on('input', function(msg) {

      var sanitizePollInterval = config.sanitizePollInterval || 3000;
      var maxPollAttempts = config.maxSanitizePollAttempts || 15;
      sanitizePool.setSize(config.sanitizePool);

      sanitizer.run(msg,(opts,cb)=>{
        //opts.node.log(JSON.stringify(Cache.all("files")));
        opts.node.status({fill:"green",shape:"ring",text:"starting sanitization"});
        if (Trail) Trail.i.p(opts.node,`sanitizing files on ${opts.config.sasaUrl}`,opts.msg);

        (function __sanitize(files,idx,cb) {
          if (idx>=files.length) return cb();
          var file=files[idx].value;
          //opts.node.log(JSON.stringify(file));
          sanitizePool.add((thread,file,__done)=>{
            opts.node.status({fill:"green",shape:"ring",text:"sanitizing: "+file.name});
            if (Trail) Trail.d.p(opts.node,`sanitizing file "${file.name}"`,opts.msg);

            var stats = fs.statSync(path.join(file.folder.tempDownloadFolder,file.name));
            if (!fs.existsSync(path.join(file.folder.tempDownloadFolder,file.name))) {
              file.state=Sanitizer.FILE_STATE.SANITIZATION_FAILED;
              opts.node.status({fill:"yellow",shape:"ring",text:"local file does not exist "+file.name});
              if (Trail) Trail.w.p(opts.node,`local file does not exist  ${path.join(file.folder.tempDownloadFolder,file.name)}: ${e}`,file);
              return __done();
            }
            var stream= fs.createReadStream(path.join(file.folder.tempDownloadFolder,file.name));
            var formData = {file: stream};
            var headers = {
              "FileName": file.name,
              "GS-Key": opts.config.sasaKey,
              "AppInfo": "Accellion SASA/1.0",
              "ProfileID": ""+opts.config.sasaProfileId,
              "F-Size": ""+stats.size,
              "ScanJob": "true",
              "Accept": "application/json"
            };
            try {
              request.post({url: opts.config.sasaUrl+"/Scanner.svc/v2/upload/file", headers: headers, body: stream},(error, response, body) => {
                try {
                  body = JSON.parse(body);
                  if (response.statusCode==200 && body.JobID && !body.JobError) {
                    //opts.node.log(JSON.stringify(body));
                    file.state=Sanitizer.FILE_STATE.SANITIZING;
                    file.sanitization={jobID:body.JobID};
                    opts.node.status({fill:"green",shape:"ring",text:"sanitization job running for file "+file.name});
                    if (Trail) Trail.d.p(opts.node,`got job ${body.JobID} for file ${file.name}`,file);

                    var attempt=0;
                    var headers = {
                      "GS-Key": opts.config.sasaKey,
                      "AppInfo": "Accellion SASA/1.0",
                      "Accept": "application/json"
                    };
                    (function __poll(cb){
                      attempt++;
                      try {
                        request.get({url: opts.config.sasaUrl+"/Scanner.svc/v2/scan/"+attempt+"/"+file.sanitization.jobID, headers: headers},(error, response, body) => {
                          try {
                            body = JSON.parse(body);
                            //opts.node.log(JSON.stringify(body));
                            if (body.Scan_Status_c==SCAN_STATE.FINISHED) {
                              if (Trail) Trail.d.p(opts.node,`sanitization job completed: ${file.sanitization.jobID}`,file);
                              if (Trail) Trail.s.p(opts.node,`sanitization job result: ${JSON.stringify(body)}`,file);
                              file.sanitization.report=body;
                              if (file.sanitization.report.ScansLogArr && file.sanitization.report.ScansLogArr.length>0) {
                                if (!fs.existsSync(file.folder.tempSanitizeFolder)) fs.mkdirSync(file.folder.tempSanitizeFolder,{recursive:true});
                                var scansLogArr = file.sanitization.report.ScansLogArr[0];
                                file.sanitization.result=scansLogArr.Result;
                                file.sanitization.resultDC=Object.entries(SCAN_RESULT).find((el)=>{return el[1]==scansLogArr.Result})[0];
                                switch (scansLogArr.Result) {
                                  case SCAN_RESULT.OK:
                                    if (Trail) Trail.d.p(opts.node,`sanitization job ${file.sanitization.jobID} for file ${file.name} returned ok`,file);
                                    file.state=Sanitizer.FILE_STATE.SANITIZED;
                                    file.sanitizedName=file.name;
                                    if (Trail) Trail.d.p(opts.node,`using original file for job ${file.sanitization.jobID} and file ${file.name}`,file);
                                    fs.copyFileSync(path.join(file.folder.tempDownloadFolder,file.name),path.join(file.folder.tempSanitizeFolder,file.name));
                                    return __done(thread,null);
                                    break;
                                  case SCAN_RESULT.DROP:
                                    if (Trail) Trail.d.p(opts.node,`sanitization job ${file.sanitization.jobID} for file ${file.name} returned dropped`,file);
                                    file.state=Sanitizer.FILE_STATE.SANITIZATION_FAILED;
                                    return __done(thread,null);
                                    break;
                                  case SCAN_RESULT.RECONSTRUCTED:
                                    if (Trail) Trail.d.p(opts.node,`sanitization job ${file.sanitization.jobID} for file ${file.name} returned reconstructed`,file);
                                    file.state=Sanitizer.FILE_STATE.SANITIZED;
                                    var downloadFilename = scansLogArr.OutputFileName;
                                    file.sanitizedName = downloadFilename.split("\\");
                                    file.sanitizedName = file.sanitizedName[file.sanitizedName.length-1];
                                    if (Trail) Trail.d.p(opts.node,`downloading file for job ${file.sanitization.jobID}: ${file.sanitizedName}`,file);
                                    var headers = {
                                      "GS-Key": opts.config.sasaKey,
                                      "AppInfo": "Accellion SASA/1.0",
                                      "Accept": "application/json",
                                      "FileName": downloadFilename,
                                      "F-Size": scansLogArr.FileNewSizeBytes
                                    };
                                    opts.node.status({fill:"green",shape:"ring",text:"downloading sanitized file: "+file.sanitizedName});
                                    request.get({url: opts.config.sasaUrl+"/Scanner.svc/v2/download/file/"+file.sanitization.jobID,headers: headers},(error, response, body) => {
                                      if (Trail) Trail.d.p(opts.node,`downloaded file for job ${file.sanitization.jobID}: ${file.sanitizedName}`,file);
                                      opts.node.status({fill:"green",shape:"ring",text:"downloading sanitized file: "+file.sanitizedName});
                                      return __done(thread,null);
                                    }).pipe(fs.createWriteStream(path.join(file.folder.tempSanitizeFolder,file.sanitizedName)));
                                    break;
                                  case SCAN_RESULT.ERROR:
                                    if (Trail) Trail.d.p(opts.node,`sanitization job ${file.sanitization.jobID} for file ${file.name} returned error`,file);
                                    file.state=Sanitizer.FILE_STATE.SANITIZATION_FAILED;
                                    return __done(thread,null);
                                    break;
                                  default:
                                    if (Trail) Trail.d.p(opts.node,`sanitization job ${file.sanitization.jobID} reported unknown scan result: ${scansLogArr.Result}`,file);
                                    file.state=Sanitizer.FILE_STATE.SANITIZATION_FAILED;
                                    return __done(thread,null);
                                    break;
                                }
                              } else {
                                file.state=Sanitizer.FILE_STATE.SANITIZATION_FAILED;
                                opts.node.status({fill:"yellow",shape:"ring",text:"no sanitization server scan result for "+file.sanitization.jobID});
                                if (Trail) Trail.w.p(opts.node,`"no sanitization server scan result for ${file.sanitization.jobID}: ${e}`,file);
                                return __done(thread,null);
                              }
                              return __done(thread,null);
                            } else {
                              if (Trail) Trail.s.p(opts.node,`sanitization job still running: ${file.sanitization.jobID}, state: ${body.Scan_Status_c}, queue: ${body.PlaceInQueue}, progress: ${body.ScanProgress}, attempt: ${attempt}`,body);
                              if (attempt >= maxPollAttempts) {
                                file.state=Sanitizer.FILE_STATE.SANITIZATION_FAILED;
                                opts.node.status({fill:"yellow",shape:"ring",text:"maximum poll attempts exceeded for "+file.sanitization.jobID});
                                if (Trail) Trail.w.p(opts.node,`maximum poll attempts exceeded for ${file.sanitization.jobID}: ${attempt}`,file);
                                return __done(thread,null);
                              } else {
                                setTimeout(()=>{__poll(cb);},sanitizePollInterval);
                              }
                            }
                          } catch (e) {
                            file.state=Sanitizer.FILE_STATE.SANITIZATION_FAILED;
                            opts.node.status({fill:"yellow",shape:"ring",text:"invalid sanitization server response for "+file.sanitization.jobID});
                            if (Trail) Trail.w.p(opts.node,`invalid sanitization server response for poll ${file.sanitization.jobID}: ${e}`,file);
                            return __done(thread,null);
                          }
                        });
                      } catch (e) {
                        file.state=Sanitizer.FILE_STATE.SANITIZATION_FAILED;
                        opts.node.status({fill:"yellow",shape:"ring",text:"sanitization poll failed for "+file.sanitization.jobID});
                        if (Trail) Trail.w.p(opts.node,`sanitization poll failed for ${file.sanitization.jobID}: ${e}`,file);
                        return __done(thread,null);
                      }
                    })(()=>{
                      return __done(thread,null);
                    });

                  } else {
                    // scan failed
                    file.state=Sanitizer.FILE_STATE.SANITIZATION_FAILED;
                    opts.node.status({fill:"yellow",shape:"ring",text:"sanitization failed for "+file.name});
                    if (Trail) Trail.w.p(opts.node,`sanitization failed for ${file.name}, response code: ${response.statusCode}, body: ${JSON.stringify(body)}`,file);
                    return __done(thread,null);
                  }
                } catch (e) {
                  //body not json
                  file.state=Sanitizer.FILE_STATE.SANITIZATION_FAILED;
                  opts.node.status({fill:"yellow",shape:"ring",text:"invalid sanitization server response for "+file.name});
                  if (Trail) Trail.w.p(opts.node,`invalid sanitization server response for ${file.name}: ${e}`,file);
                  return __done(thread,null);
                }
              });
            } catch (e) {
              //post failed
              file.state=Sanitizer.FILE_STATE.SANITIZATION_FAILED;
              opts.node.status({fill:"yellow",shape:"ring",text:"sanitization request failed for "+file.name});
              if (Trail) Trail.w.p(opts.node,`sanitization request failed for ${file.name}: ${e}`,file);
              return __done(thread,null);
            }
          },file);
          idx++;
          __sanitize(files,idx,cb);
        }(sanitizer.filterFiles(Sanitizer.FILE_STATE.DOWNLOADED),0,()=>{
          // wait for the files
          (function waitForSanitization(){
            if (sanitizer.filterFiles(Sanitizer.FILE_STATE.DOWNLOADED).length==0 && sanitizer.filterFiles(Sanitizer.FILE_STATE.SANITIZING).length==0) {
              if (Trail) Trail.i.p(opts.node,`sanitization done`,null);
              return cb();
            }
            var completed=sanitizer.filterFiles(Sanitizer.FILE_STATE.SANITIZED).length+sanitizer.filterFiles(Sanitizer.FILE_STATE.SANITIZATION_FAILED).length;
            var remaining=sanitizer.filterFiles(Sanitizer.FILE_STATE.DOWNLOADED).length+sanitizer.filterFiles(Sanitizer.FILE_STATE.SANITIZING).length;
            opts.node.status({fill:"green",shape:"ring",text:"sanitized: "+completed+", remaining: "+remaining});
            setTimeout(waitForSanitization,250);
          }());
        }));
      });
      //------------------------------------------------------------------------
    });
    //--------------------------------------------------------------------------
  }
  //----------------------------------------------------------------------------
  //============================================================================
  RED.nodes.registerType("accellion.sanitize.sasa",Acc_Sanitize_Sasa);
  //============================================================================
}
//==============================================================================
