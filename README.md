#node-red-contrib-accellion-custom-cdr
### Custom Accellion Nodes for CDR Sanitization
Pulls content from one Accellion folder, pushes it to a CDR platform and pulls the results back into another Accellion folder or a different Accellion platform.

Currently provides one node:
*accellion.sanitize.sasa*
  which integrates with the [Sasa] (https://www.sasa-software.com) sanitization platform.
 
For more information on the Accellion Content Firewall, please visit [Accellion](https://www.accellion.com). 
 
 
 
