const path = require('path');
const fs = require('fs');
const request = require('request');
const rimraf = require('rimraf');
const moment = require('moment');
const requirex = require('./requirex.js');
//==============================================================================
const Trail=requirex("trail",["node-red-contrib-accellion","accellion.node-red"]);
//==============================================================================
const Cache=require("./cache.js");
const Threads=require("./threads.js");
//const PathSeparator=path.sep=="\\" ? "\\\\" : path.sep;
//==============================================================================
class Sanitizer {
  //============================================================================
  constructor(config,node,RED) {
    this.running=false;
    this.config=config;
    this.node=node;
    this.RED=RED;
    this.cache=new Cache();
    this.downloadPool = Threads.createPool(node.id+"_download", config.apiPool || 10).run();
    this.deletePool   = Threads.createPool(node.id+"_delete",   config.apiPool || 10).run();
    this.uploadPool   = Threads.createPool(node.id+"_upload",   config.apiPool || 10).run();
  }
  //============================================================================
  run(msg,sanitize) {
    var self=this;
    if (self.running) {
      if (Trail) Trail.w.p(self.node,`sanitizer still running, ignoring request to start`,msg);
      return;
    }
    self.running=true;
    self.downloadPool.setSize(self.config.apiPool);
    self.deletePool.setSize(self.config.apiPool);
    self.uploadPool.setSize(self.config.apiPool);

    if (self.config.clearCache) {
      self.cache.clear("folders");
      self.cache.clear("files");
    }
    if (Trail) Trail.i.p(self.node,`starting sanitizer`,msg);
    self.node.status({fill:"green",shape:"ring",text:"starting sanitizer"});
    list({node:self.node,config:self.config,msg:msg},()=>{                      // list source folder structure & objects (files)
      download({node:self.node,config:self.config,msg:msg},()=>{                // download to tempDownloadFolder
        sanitize({node:self.node,config:self.config,msg:msg},()=>{              // execute the sanitization callback
          structure({node:self.node,config:self.config,msg:msg},()=>{           // create structure on destination
            upload({node:self.node,config:self.config,msg:msg},()=>{            // upload files to the destination from tempSanitizeFolder
              remove({node:self.node,config:self.config,msg:msg},()=>{          // remove files from source
                msg.files = self.cache.all("files").map((f)=>{return f.value});
                self.state(msg.files);

                var failures = msg.files.filter((f)=>{return f.state!=Sanitizer.FILE_STATE.DONE});
                self.node.status({fill:"green",shape:"dot",text:"done, "+(msg.files.length-failures.length)+" transfers, "+failures.length+" failures"});

                if (failures.length>0) {
                  try {
                    if (!fs.existsSync(self.config.failureLogFolder)) fs.mkdirSync(self.config.failureLogFolder,{recursive:true});
                    fs.writeFileSync(path.join(self.config.failureLogFolder,moment().format("YYYY-MM-DD_HH-mm-ss")+"_failures.json"), JSON.stringify(failures,null,3));
                  } catch (err) {
                    self.node.status({fill:"yellow",shape:"dot",text:"could not save to failure file: "+err});
                  }
                }
                self.running=false;
                return self.node.send(msg);
              });
            });
          });
        });
      });
    });
    //--------------------------------------------------------------------------
    // LIST
    //--------------------------------------------------------------------------
    function list(opts,cb) {
      if (Trail) Trail.i.p(opts.node,`listing objects from ${opts.config.sourceFolder}`,msg);
      var kw = self.RED.nodes.getNode(opts.config.sourceToken).kw;
      opts.node.status({fill:"green",shape:"ring",text:"finding source root"});
      var folder=self.cache.find("folders","/");
      if (folder) {
        opts.node.status({fill:"green",shape:"ring",text:"found root folder"});
        if (Trail) Trail.s.p(opts.node,`root folder in cache ${opts.config.sourceFolder}`,msg);
        startFolderIteration(folder,cb);
      } else {
        if (Trail) Trail.s.p(opts.node,`root folder not cached ${opts.config.sourceFolder}`,msg);
        kw.folders.find.byName(opts.config.sourceFolder).then((folder)=>{
          folder = {
            sourceFolder:"/"+folder.path,
            sourceId:folder.id,
            destinationFolder:opts.config.destinationFolder,
            destinationId:null,
            tempDownloadFolder: path.join(opts.config.tempFolder,"downloads"),//.replace(/\//g,separator),
            tempSanitizeFolder: path.join(opts.config.tempFolder,"sanitized"),//.replace(/\//g,separator),
            relative: "/"
          };
          opts.node.status({fill:"green",shape:"ring",text:"found root folder"});
          if (Trail) Trail.s.p(opts.node,`found root folder ${folder.sourceFolder}, caching`,msg);
          self.cache.set("folders","/",folder);
          startFolderIteration(folder,cb);
        },(err)=>{
          opts.node.status({fill:"red",shape:"dot",text:"error "+err});
          if (Trail) Trail.e.h(opts.node,`error looking for root folder "${opts.config.sourceFolder}": ${err}`,err);
          return opts.node.error("an error occurred: "+err,msg);
        });
      }
      //------------------------------------------------------------------------
      function startFolderIteration(folder,cb) {
        opts.node.status({fill:"green",shape:"ring",text:"listing source files"});
        if (Trail) Trail.s.p(opts.node,`starting folder iteration ${folder.sourceFolder}`,msg);
        self.cache.clear("files");
        var folders=[];

        (function iterate(folder,done) {
          opts.node.status({fill:"green",shape:"ring",text:"listing children for "+folder.sourceFolder});
          if (Trail) Trail.s.p(opts.node,`finding children for ${folder.sourceFolder}`,msg);
          kw.folders.children.byId(folder.sourceId).then((list)=>{
            opts.node.status({fill:"green",shape:"ring",text:"checking "+list.data.length+" objects..."});
            if (Trail) Trail.d.p(opts.node,`found ${list.data.length} children for ${folder.sourceFolder}`,msg);
            var __files = list.data.filter((el)=>{return el.type=='f' && !el.deleted && !el.permDeleted});
            var __folders=list.data.filter((el)=>{return el.type=='d' && !el.deleted && !el.permDeleted});

            for (var i = 0; i < __files.length; i++) {
              var __file={
                name: __files[i].name,
                sanitizedName: __files[i].name,
                sourceId: __files[i].id,
                folder: folder,
                state:Sanitizer.FILE_STATE.IDENTIFIED,
                sanitization: null
              }
              self.cache.set("files",__files[i].id,__file);
            }

            for (var i = 0; i < __folders.length; i++) {
              var __relative = "/"+__folders[i].path.substring(opts.config.sourceFolder.length);
              var __folder = self.cache.find("folders",__relative);
              if (!__folder) {
                var __folder = {
                  sourceFolder:"/"+__folders[i].path,
                  sourceId:__folders[i].id,
                  destinationFolder:opts.config.destinationFolder+__relative,
                  destinationId:null,
                  tempDownloadFolder: path.join(opts.config.tempFolder,"downloads",__relative),//.replace(/\//g,separator),
                  tempSanitizeFolder: path.join(opts.config.tempFolder,"sanitized",__relative),//.replace(/\//g,separator),
                  relative: __relative
                };
                self.cache.set("folders",__relative,__folder);
              }
              folders.push(__folder);
            }

            if (folders.length==0) {
              opts.node.status({fill:"green",shape:"ring",text:"done iterating "+opts.config.sourceFolder});
              if (Trail) Trail.s.p(opts.node,`done iterating ${opts.config.sourceFolder}`,msg);
              return done();
            }
            iterate(folders.shift(),done);

          },(err)=>{
            opts.node.status({fill:"red",shape:"dot",text:"error "+err+" on "+folder.sourceFolder});
            if (Trail) Trail.e.h(opts.node,`could not list children for "${folder.sourceFolder}": ${err}`,err);
            return opts.node.error("error: "+err,msg);
          });
        }(folder,()=>{
          if (Trail) Trail.i.p(opts.node,`listing done`,null);
          return cb();
        }));
      }
      //------------------------------------------------------------------------
    }
    //--------------------------------------------------------------------------
    // DOWNLOAD
    //--------------------------------------------------------------------------
    function download(opts,cb) {
      var kw = self.RED.nodes.getNode(opts.config.sourceToken).kw;
      opts.node.status({fill:"green",shape:"ring",text:"starting download"});
      if (Trail) Trail.i.p(opts.node,`downloading from ${opts.config.sourceFolder}`,opts.msg);
      if (!fs.existsSync(opts.config.tempFolder)) fs.mkdirSync(opts.config.tempFolder,{recursive:true});

      (function __download(files,idx,cb){
        if (idx>=files.length) return cb();
        var file=files[idx].value;
        //opts.node.log(JSON.stringify(file));
        self.downloadPool.add((thread,file,__done)=>{
          if (!fs.existsSync(file.folder.tempDownloadFolder)) fs.mkdirSync(file.folder.tempDownloadFolder,{recursive:true});
          opts.node.status({fill:"green",shape:"ring",text:"downloading: "+file.name});
          if (Trail) Trail.d.p(opts.node,`downloading file "${file.name}"`,opts.msg);
          kw.files.download.byId(file.sourceId,file.folder.tempDownloadFolder,file.name).then((res)=>{
            file.state=Sanitizer.FILE_STATE.DOWNLOADED;
            opts.node.status({fill:"green",shape:"ring",text:"downloaded: "+file.name});
            if (Trail) Trail.d.p(opts.node,`downloaded file "${file.name}"`,opts.msg);
            __done(thread,null);
          },(err)=>{
            file.state=Sanitizer.FILE_STATE.DOWNLOAD_FAILED;
            opts.node.status({fill:"yellow",shape:"ring",text:"error: "+err});
            if (Trail) Trail.w.p(opts.node,`could not download file "${file.name}" with id ${file.sourceId}: ${err}`,err);
            __done(thread,null);
          });
        },file);
        idx++;
        __download(files,idx,cb);
      }(self.cache.all("files"),0,()=>{
        // wait for the files
        (function waitForDownloads(){
          if (self.filterFiles(Sanitizer.FILE_STATE.IDENTIFIED).length==0) {
            if (Trail) Trail.i.p(opts.node,`downloads done`,null);
            return cb();
          }
          if (self.filterFiles(Sanitizer.FILE_STATE.DOWNLOADED).length>0) opts.node.status({fill:"green",shape:"ring",text:"downloaded: "+self.filterFiles(Sanitizer.FILE_STATE.DOWNLOADED).length});
          setTimeout(waitForDownloads,250);
        }());
      }));

    }
    //--------------------------------------------------------------------------
    // STRUCTURE
    //--------------------------------------------------------------------------
    function structure(opts,cb) {
      var kw = self.RED.nodes.getNode(opts.config.destinationToken).kw;
      opts.node.status({fill:"green",shape:"ring",text:"starting structure creation"});
      if (Trail) Trail.i.p(opts.node,`creating structure for ${opts.config.destinationFolder}`,opts.msg);

      (function __structure(files,idx,cb){
        if (idx>=files.length) return cb();
        var file=files[idx].value;
        //create structure recursively...
        //opts.node.log(JSON.stringify(self.cache.all("folders")));
        //opts.node.log(JSON.stringify(self.cache.all("files")));
        var folder=self.cache.find("folders",file.folder.relative);
        if (!folder) {
          if (Trail) Trail.w.p(opts.node,`folder exists in file cache, but not in folder cache`,null);
          folder=file.folder;
          self.cache.set("folders",file.folder.relative,folder);
        }
        if (folder.destinationId && !opts.config.ignoreDestinationCache) {
          if (Trail) Trail.d.p(opts.node,`folder exists in cache ${folder.destinationFolder}`,null);
          file.state=Sanitizer.FILE_STATE.STRUCTURE_CREATED;
          idx++;
          __structure(files,idx,cb);
        } else {

          //do we have an id for the root folder?
          var rootFolder=self.cache.find("folders","/");
          if (!rootFolder.destinationId) {
            kw.folders.find.byName(opts.config.destinationFolder).then((__folder)=>{
              if (Trail) Trail.d.p(opts.node,`got destination root for ${opts.config.destinationFolder}`,null);
              opts.node.status({fill:"green",shape:"ring",text:"got destination root for "+opts.config.destinationFolder});
              rootFolder.destinationId=__folder.id;
              __startDestIteration(rootFolder.destinationId,()=>{idx++;__structure(files,idx,cb);});
            },(err)=>{
              if (Trail) Trail.w.p(opts.node,`cannot find destination root: ${err}`,null);
              opts.node.status({fill:"red",shape:"dot",text:"cannot find destination root"});
              return cb();
            });
          } else {
            __startDestIteration(rootFolder.destinationId,()=>{idx++;__structure(files,idx,cb);});
          }
          //--------------------------------------------------------------------
          function __startDestIteration(parentId,__diCb,idx) {
            var destArr = file.folder.destinationFolder.split("/");
            if (!idx) idx=opts.config.destinationFolder.split("/").length;
            if (idx>=destArr.length) {
              opts.node.status({fill:"green",shape:"ring",text:"structure created for "+file.folder.destinationFolder});
              if (Trail) Trail.d.p(opts.node,`structure created for ${file.folder.destinationFolder}`,null);
              file.state=Sanitizer.FILE_STATE.STRUCTURE_CREATED;
              return __diCb();
            }
            //check if folder exists in parent objects
            kw.folders.children.byId(parentId).then((list)=>{
              opts.node.status({fill:"green",shape:"ring",text:"checking "+list.data.length+" objects..."});
              if (Trail) Trail.d.p(opts.node,`found ${list.data.length} children for ${destArr[idx]}`,msg);

              var __childFolders=list.data.filter((el)=>{return el.type=='d' && !el.deleted && !el.permDeleted});
              var matching = __childFolders.find((el)=>{return el.name==destArr[idx]});
              if (matching && idx==destArr.length-1) {
                //folder exists and it's the final one
                folder.destinationId=matching.id;
                opts.node.status({fill:"green",shape:"ring",text:"structure created for "+file.folder.destinationFolder});
                if (Trail) Trail.d.p(opts.node,`structure created for ${file.folder.destinationFolder}`,null);
                file.state=Sanitizer.FILE_STATE.STRUCTURE_CREATED;
                return __diCb();
              } else if (matching) {
                //folder exists, continue iterating
                opts.node.status({fill:"green",shape:"ring",text:"found folder "+matching.name});
                if (Trail) Trail.d.p(opts.node,`structure created for ${matching.name}`,null);
                idx++;
                return __startDestIteration(matching.id,__diCb,idx);
              } else {
                //we need to create the folder
                kw.folders.add.byId({parentId:parentId,name:destArr[idx]}).then((el)=>{
                  if (idx==destArr.length-1) {
                    //folder created and it's the final one
                    folder.destinationId=el.id;
                    opts.node.status({fill:"green",shape:"ring",text:"structure created for "+file.folder.destinationFolder});
                    if (Trail) Trail.d.p(opts.node,`structure created for ${file.folder.destinationFolder}`,null);
                    file.state=Sanitizer.FILE_STATE.STRUCTURE_CREATED;
                    return __diCb();
                  } else {
                    //folder created, continue iterating
                    opts.node.status({fill:"green",shape:"ring",text:"created folder "+el.name});
                    if (Trail) Trail.d.p(opts.node,`folder created ${el.name}`,null);
                    idx++;
                    return __startDestIteration(el.id,__diCb,idx);
                  }
                },(err)=>{
                  if (Trail) Trail.w.p(opts.node,`cannot create folder ${destArr[idx]}: ${err}`,null);
                  opts.node.status({fill:"red",shape:"dot",text:"cannot create folder "+destArr[idx]});
                  file.state=Sanitizer.FILE_STATE.STRUCTURE_FAILED;
                  return __diCb();
                });
              }
            },(err)=>{
              if (Trail) Trail.w.p(opts.node,`cannot iterate structure for ${destArr[idx]}: ${err}`,null);
              opts.node.status({fill:"red",shape:"dot",text:"cannot iterate structure"});
              file.state=Sanitizer.FILE_STATE.STRUCTURE_FAILED;
              return __diCb();
            });
          }
          //--------------------------------------------------------------------
        }
      }(self.filterFiles(Sanitizer.FILE_STATE.SANITIZED),0,()=>{
        // wait for the files
        (function waitForStructure(){
          if (self.filterFiles(Sanitizer.FILE_STATE.SANITIZED).length==0) {
            if (Trail) Trail.i.p(opts.node,`structure done`,null);
            return cb();
          }
          if (self.filterFiles(Sanitizer.FILE_STATE.STRUCTURE_CREATED).length>0) opts.node.status({fill:"green",shape:"ring",text:"structured: "+self.filterFiles(Sanitizer.FILE_STATE.STRUCTURE_CREATED).length});
          setTimeout(waitForStructure,250);
        }());
      }));
    }
    //--------------------------------------------------------------------------
    // UPLOAD
    //--------------------------------------------------------------------------
    function upload(opts,cb) {
      var kw = self.RED.nodes.getNode(opts.config.destinationToken).kw;
      opts.node.status({fill:"green",shape:"ring",text:"starting upload"});
      if (Trail) Trail.i.p(opts.node,`uploading to ${opts.config.destinationFolder}`,opts.msg);

      (function __upload(files,idx,cb){
        if (idx>=files.length) return cb();
        var file=files[idx].value;
        //opts.node.log(JSON.stringify(file));
        self.uploadPool.add((thread,file,__done)=>{
          opts.node.status({fill:"green",shape:"ring",text:"uploading: "+file.sanitizedName});
          if (Trail) Trail.d.p(opts.node,`uploading file "${file.sanitizedName}"`,opts.msg);

          var folderId=self.cache.find("folders",file.folder.relative).destinationId;
          kw.files.upload.byFolderId(folderId,path.join(file.folder.tempSanitizeFolder,file.sanitizedName)).then((res)=>{
            file.state=Sanitizer.FILE_STATE.UPLOADED;
            opts.node.status({fill:"green",shape:"ring",text:"uploaded: "+file.sanitizedName});
            if (Trail) Trail.d.p(opts.node,`uploaded file "${file.sanitizedName}"`,opts.msg);
            __done(thread,null);
          },(err)=>{
            file.state=Sanitizer.FILE_STATE.UPLOAD_FAILED;
            opts.node.status({fill:"yellow",shape:"ring",text:"error: "+err});
            if (Trail) Trail.w.p(opts.node,`could not upload file "${file.sanitizedName}": ${err}`,err);
            __done(thread,null);
          });
        },file);
        idx++;
        __upload(files,idx,cb);
      }(self.filterFiles(Sanitizer.FILE_STATE.STRUCTURE_CREATED),0,()=>{
        // wait for the files
        (function waitForUploads(){
          if (self.filterFiles(Sanitizer.FILE_STATE.STRUCTURE_CREATED).length==0) {
            if (Trail) Trail.i.p(opts.node,`uploads done`,null);
            return cb();
          }
          if (self.filterFiles(Sanitizer.FILE_STATE.UPLOADED).length>0) opts.node.status({fill:"green",shape:"ring",text:"uploaded: "+self.filterFiles(Sanitizer.FILE_STATE.UPLOADED).length});
          setTimeout(waitForUploads,250);
        }());
      }));
    }
    //--------------------------------------------------------------------------
    // REMOVE
    //--------------------------------------------------------------------------
    function remove(opts,cb) {
      var kw = self.RED.nodes.getNode(opts.config.sourceToken).kw;
      opts.node.status({fill:"green",shape:"ring",text:"starting removal"});
      if (Trail) Trail.i.p(opts.node,`deleting from ${opts.config.sourceFolder}`,opts.msg);

      (function __remove(files,idx,cb){
        if (idx>=files.length) return cb();
        var file=files[idx].value;
        //opts.node.log(JSON.stringify(file));
        self.deletePool.add((thread,file,__done)=>{
          opts.node.status({fill:"green",shape:"ring",text:"removing: "+file.name});
          if (Trail) Trail.d.p(opts.node,`removing file "${file.name}"`,opts.msg);
          kw.files.delete.byId(file.sourceId).then((res)=>{
            file.state=Sanitizer.FILE_STATE.DONE;
            opts.node.status({fill:"green",shape:"ring",text:"removed: "+file.name});
            if (Trail) Trail.d.p(opts.node,`removed file "${file.name}"`,opts.msg);
            __done(thread,null);
          },(err)=>{
            removed++;
            opts.node.status({fill:"yellow",shape:"ring",text:"error: "+err});
            if (Trail) Trail.w.p(opts.node,`could not remove file "${file.name}" with id ${file.sourceId}: ${err}`,err);
            __done(thread,null);
          });
        },file);
        idx++;
        __remove(files,idx,cb);
      }(self.filterFiles(Sanitizer.FILE_STATE.UPLOADED),0,()=>{
        // wait for the files
        (function waitForRemovals(){
          if (self.filterFiles(Sanitizer.FILE_STATE.UPLOADED).length==0) {
            rimraf.sync(path.join(opts.config.tempFolder,"downloads"));
            rimraf.sync(path.join(opts.config.tempFolder,"sanitized"));
            if (Trail) Trail.i.p(opts.node,`removals done`,null);
            return cb();
          }
          opts.node.status({fill:"green",shape:"ring",text:"removed: "+self.filterFiles(Sanitizer.FILE_STATE.DONE).length+" files"});
          setTimeout(waitForRemovals,250);
        }());
      }));
    }
    //--------------------------------------------------------------------------
  }
  //----------------------------------------------------------------------------
  //----------------------------------------------------------------------------
  // HELPERS
  //----------------------------------------------------------------------------
  filterFiles(state) {
    return this.cache.all("files").filter((file)=>{return file.value.state==state});
  }
  //----------------------------------------------------------------------------
  state(files) {
    for (var i = 0; i < files.length; i++) {
      var file=files[i];
      file.stateDC=Object.entries(Sanitizer.FILE_STATE).find((el)=>{return el[1]==file.state})[0];
    }
  }
  //----------------------------------------------------------------------------
  //============================================================================
}
//==============================================================================
Sanitizer.FILE_STATE={
  "IDENTIFIED":1,
  "DOWNLOADED":2,
  "DOWNLOAD_FAILED":3,
  "SANITIZING": 4,
  "SANITIZED":5,
  "SANITIZATION_FAILED":6,
  "STRUCTURE_CREATED":7,
  "STRUCTURE_FAILED":8,
  "DONE":9,
  "UPLOADED":10,
  "UPLOAD_FAILED":11,
}
//==============================================================================
module.exports=Sanitizer;
