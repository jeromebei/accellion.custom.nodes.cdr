const fs=require("fs");
const path=require("path");
//==============================================================================
var requirex=function(name,relPathArr) {
  var back="";
  var check="";
  for (var i = 0; i < 5; i++) {
    back = path.join(back,"..");
    for (var j = 0; j < relPathArr.length; j++) {
      var relPath = relPathArr[j];
      check = path.join(__dirname,back,relPath,name+".js");
      if (fs.existsSync(check)) return require(check);
      check = path.join(__dirname,back,"node_modules",relPath,name+".js");
      if (fs.existsSync(check)) return require(check);
    }
  }
  console.error("not found",check);
}
//==============================================================================
module.exports=requirex;
