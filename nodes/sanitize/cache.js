class Cache {
  //============================================================================
  constructor(){
    this.__caches=[];
  }
  //============================================================================
  find(cacheName,objectKey){
    var cache=this.__caches.find((o)=>{return o.name==cacheName});
    if (!cache) return null;
    var object = cache.objects.find((o)=>{return o.key==objectKey});
    if (object) return object.value;
    return null;
  }
  //============================================================================
  all(cacheName){
    var cache=this.__caches.find((o)=>{return o.name==cacheName});
    if (!cache) return [];
    return cache.objects;
  }
  //============================================================================
  clear(cacheName){
    var cache=this.__caches.find((o)=>{return o.name==cacheName});
    if (!cache) return;
    cache.objects=[];
  }
  //============================================================================
  set(cacheName,objectKey,objectValue) {
    var cache=this.__caches.find((o)=>{return o.name==cacheName});
    if (!cache) {
      cache={name:cacheName,objects:[{key:objectKey,value:objectValue}]};
      this.__caches.push(cache);
    } else {
      var object = cache.objects.find((o)=>{return o.key==objectKey});
      if (object) object.value=objectValue;
      else cache.objects.push({key:objectKey,value:objectValue});
    }
  }
  //============================================================================
}

module.exports=Cache;
