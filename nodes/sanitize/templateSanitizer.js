const path = require('path');
const fs = require('fs');
const request = require('request');
const rimraf = require('rimraf');
const requirex = require('./requirex.js');
//==============================================================================
const Trail=requirex("trail",["node-red-contrib-accellion","accellion.node-red"]);
//==============================================================================
const Threads=require("./threads.js");
const Sanitizer=require("./sanitizer.js");
//==============================================================================
module.exports = function(RED) {
  //============================================================================
  //============================================================================
  function Acc_Sanitize_Template(config) {
    RED.nodes.createNode(this,config);
    var node=this;
    var sanitizer = new Sanitizer(config,node,RED);
    var sanitizePool = Threads.createPool(node.id+"_sanitize", config.sanitizePool || 10).run();
    //--------------------------------------------------------------------------
    // INPUT
    //--------------------------------------------------------------------------
    node.on('input', function(msg) {

      sanitizePool.setSize(config.sanitizePool);

      sanitizer.run(msg,(opts,cb)=>{
        opts.node.status({fill:"green",shape:"ring",text:"starting sanitization"});
        if (Trail) Trail.i.p(opts.node,`sanitizing files on ${opts.config.templateUrl}`,opts.msg);

        /*
        //----------------------------------------------------------------------
        sanitization process
        //----------------------------------------------------------------------

        The sanitizer.run needs to be called with the msg object and a callback function (in which this comment is written).
        The sanitizer.run will
          1) list the files in the configured accellion folder                      -> file.state: Sanitizer.FILE_STATE.IDENTIFIED
          2) download these files to a local temporary folder                       -> file.state: Sanitizer.FILE_STATE.DOWNLOADED
          3) *** execute the sanitization callback we're in right now ***           -> file.state: Sanitizer.FILE_STATE.SANITIZED
          4) create the same folder structure on the accellion destination folder   -> file.state: Sanitizer.FILE_STATE.STRUCTURE_CREATED
          5) upload the sanitized files into that structure                         -> file.state: Sanitizer.FILE_STATE.UPLOADED;
          6) remove the original file from the accellion source folder              -> file.state: Sanitizer.FILE_STATE.DONE;

        //----------------------------------------------------------------------
        sanitizer.run callback function signature
        //----------------------------------------------------------------------

        (opts,cb)=>{}

        the opts object has the following properties:
          - node:   the calling node object
          - config  the calling node's configuration
          - msg     the calling node's message object

        cb represents the function you need to call after your sanitization process is completed for all files to continue the structrure and upload process.
        Make sure your entire processing is done before calling cb, especially when you are working with multiple threads.

        //----------------------------------------------------------------------
        callback implementation notes
        //----------------------------------------------------------------------

        1)  Iterate through all the files that are in state "DOWNLOADED".
            You can get a list of downloaded files like so:
            var downloadedFiles = sanitizer.filterFiles(Sanitizer.FILE_STATE.DOWNLOADED);

        2)  For each file in the list, set the file state property to Sanitizer.FILE_STATE.SANITIZING, then
            send (upload) it to your sanitization solution and process it.
            The local file path can be found in the property file.folder.tempDownloadFolder,
            and its name will be in file.name
            You can do this in a multi-threaded way by using the sanitizePool (configured above).


        3)  When the files come back from your sanitization solution (you might need to poll depending on your technology),
            assign the correct file state to the file object:

              Sanitizer.FILE_STATE.SANITIZATION_FAILED
              Sanitizer.FILE_STATE.SANITIZED

            Set the sanitizedName property of the file object to the new file name / extension after sanitization.
            If the file name / extension remains the same, simply set the sanitizedName property to the initial file name value.

            You can also attach any sanitization result details to each file object that is needed for logging / auditing.

            If during sanitization, the file is dropped (can't be sanitized) you also need to set the file state property to Sanitizer.FILE_STATE.SANITIZATION_FAILED.

            Upon successful sanitization (and if the sanitized file itself is different from the original file), download the file into the
            "file.folder.tempSanitizeFolder" path, and name it "file.sanitizedName".
            If the sanitized file hasn't changed (is the same as the original file), you can locally copy the original file from file.folder.tempDownloadFolder into
            "file.folder.tempSanitizeFolder" path, and name it "file.sanitizedName".

        4)  Once all files have been processed, execute the callback cb() from the sanitizer.run(msg,opts,cb=>{}) function signature.

        //----------------------------------------------------------------------
        File States (Sanitizer.FILE_STATE.???)
        //----------------------------------------------------------------------

        "IDENTIFIED":             File has been identified on the accellion source to be saniotized on the current iteration
        "DOWNLOADED":             File has successfully been downloaded from the accelion source and is ready for sanitization
        "DOWNLOAD_FAILED":        File download failed from the accellion source
        "SANITIZING":             File is in sanitization process
        "SANITIZED":              File has been sanitized
        "SANITIZATION_FAILED":    File sanitization has failed
        "STRUCTURE_CREATED":      File upload structure (folder structure) has been created on the accellion destination
        "STRUCTURE_FAILED":       File structure creation has failed on the accellion destination
        "DONE":                   File has been sucessfully processed (sanitized file is uploaded and original source has been deleted)
        "UPLOADED":               File has been uploaded to the accellion destination
        "UPLOAD_FAILED":          File upload to the accellion destination has failed

        */
      });
      //------------------------------------------------------------------------
    });
    //--------------------------------------------------------------------------
  }
  //----------------------------------------------------------------------------
  //============================================================================
  RED.nodes.registerType("accellion.sanitize.template",Acc_Sanitize_Template);
  //============================================================================
}
//==============================================================================
